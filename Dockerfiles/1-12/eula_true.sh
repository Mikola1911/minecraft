#!/bin/sh

eula_txt=/eula.txt
config=/server.properties

eula=true
online=false

sed -i "s|eula=.*|eula=$eula|g" $eula_txt
sed -i "s|online-mode=.*|online-mode=$online|g" $config
